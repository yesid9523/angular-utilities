import { Component, OnInit } from '@angular/core';
import { Satelite } from '../shared/interfaces/satelite';
import { ServicioEjemploService } from '../shared/services/servicio-ejemplo.service';

@Component({
  selector: 'app-servicios-manejo',
  templateUrl: './servicios-manejo.component.html',
  styleUrls: ['./servicios-manejo.component.scss']
})
export class ServiciosManejoComponent implements OnInit {

  satelite: Satelite = {
    id: 5,
    nombre: 'Bianca',
    planeta: 'Urano'
  }

  satelitesObtenidos: Satelite [];

  constructor(private satelitessServicio: ServicioEjemploService) { }

  ngOnInit(): void {
  }

  agregarSatelite(){
    this.satelitessServicio.agregarSateliteServicio(this.satelite)
  }
    
  obtenerSatelites(){
    return this.satelitessServicio.obtenerSatelitesServicio();
  }

  distancia(distancia: string){
    return this.satelitessServicio.obtenerDistancia(distancia);
  }
}
