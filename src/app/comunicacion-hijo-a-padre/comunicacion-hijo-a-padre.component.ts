import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-comunicacion-hijo-a-padre',
  templateUrl: './comunicacion-hijo-a-padre.component.html',
  styleUrls: ['./comunicacion-hijo-a-padre.component.scss']
})
export class ComunicacionHijoAPadreComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  planetas = ['Mercurio', 'Venus', 'Tierra', 'Marte'];

  agregarPlaneta(nuevoPlaneta: any) {
    this.planetas.push(nuevoPlaneta);
  }

}
