import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComunicacionHijoAPadreComponent } from './comunicacion-hijo-a-padre.component';

describe('ComunicacionHijoAPadreComponent', () => {
  let component: ComunicacionHijoAPadreComponent;
  let fixture: ComponentFixture<ComunicacionHijoAPadreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComunicacionHijoAPadreComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ComunicacionHijoAPadreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
