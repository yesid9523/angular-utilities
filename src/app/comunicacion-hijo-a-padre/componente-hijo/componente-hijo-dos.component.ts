import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-componente-hijo-dos',
  templateUrl: './componente-hijo-dos.component.html',
  styleUrls: ['./componente-hijo-dos.component.scss']
})
export class ComponenteHijoDosComponent implements OnInit {

  @Output()  listaDePlanetas = new EventEmitter<String>();

  constructor() { }

  ngOnInit(): void {
  }

  agregarPlanetas(value: String) {
    this.listaDePlanetas.emit(value)
  }

}
