import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directiva-ng-style',
  templateUrl: './directiva-ng-style.component.html',
  styleUrls: ['./directiva-ng-style.component.scss']
})
export class DirectivaNgStyleComponent implements OnInit {

  widthExp?: String = "200";
  constructor() { }

  tamano: any = 30;
  presentacion = {
    "background-color":"black",
    "color":"white",
    "width.px":"1000",
    "height.px":"200",
    "font-size.px":this.tamano,
    "display": "flex",
    "justify-content":"center",
    "align-items": "center"
  }

  ngOnInit(): void {
  }

}
