import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directiva-ng-class',
  templateUrl: './directiva-ng-class.component.html',
  styleUrls: ['./directiva-ng-class.component.scss']
})
export class DirectivaNgClassComponent implements OnInit {

  claseGenerica: String = "fourth";
  operacionBooleana: Boolean = true;
  objetoDeClases = {
    'first': true,
    'fourth': true
  }

  arrayDeClases = ['second', 'fourth']
  constructor() { }

  ngOnInit(): void {
  }

}
