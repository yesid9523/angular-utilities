import { Component, OnInit } from '@angular/core';
import { Planeta } from '../shared/interfaces/planeta';

@Component({
  selector: 'app-comunicacion-padre-a-hijo',
  templateUrl: './comunicacion-padre-a-hijo.component.html',
  styleUrls: ['./comunicacion-padre-a-hijo.component.scss']
})
export class ComunicacionPadreAHijoComponent implements OnInit {

  planetas: Planeta[] = [
    {
      id: 1,
      nombre: "Mercurio"
    },
    {
      id: 2,
      nombre: "Venus"
    },
    {
      id: 3,
      nombre: "Tierra"
    },
    {
      id: 4,
      nombre: "Marte"
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
