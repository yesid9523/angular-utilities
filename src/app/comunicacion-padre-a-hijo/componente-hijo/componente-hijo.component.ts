import { Component, Input, OnInit } from '@angular/core';
import { Planeta } from 'src/app/shared/interfaces/planeta';

@Component({
  selector: 'app-componente-hijo',
  templateUrl: './componente-hijo.component.html',
  styleUrls: ['./componente-hijo.component.scss']
})
export class ComponenteHijoComponent implements OnInit {

  @Input() planetasDeHijo:Planeta;

  @Input() indice:Number;

  
  constructor() { }

  ngOnInit(): void {
  }

}
