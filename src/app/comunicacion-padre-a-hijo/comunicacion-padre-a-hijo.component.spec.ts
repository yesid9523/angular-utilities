import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComunicacionPadreAHijoComponent } from './comunicacion-padre-a-hijo.component';

describe('ComunicacionPadreAHijoComponent', () => {
  let component: ComunicacionPadreAHijoComponent;
  let fixture: ComponentFixture<ComunicacionPadreAHijoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComunicacionPadreAHijoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ComunicacionPadreAHijoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
