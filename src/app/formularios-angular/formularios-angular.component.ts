import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

export interface User {
  name: string;
  password: string;
  passwordRepeat: string,
}

@Component({
  selector: 'app-formularios-angular',
  templateUrl: './formularios-angular.component.html',
  styleUrls: ['./formularios-angular.component.scss'],
})
export class FormulariosAngularComponent implements OnInit {
  formGroupEjemplo: FormGroup;



  formGroupBuilderEjemplo: FormGroup;
  // formBuilderEjemplo: FormBuilder;




  constructor(private formBuilderEjemplo: FormBuilder) {
    this.formGroup();

    this.crearformBuilder();
  }
  
  formGroup(){
    this.formGroupEjemplo = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(6)]),
      password: new FormControl(''),
      passwordRepeat: new FormControl(''),
    });
  }

  crearformBuilder(){
    this.formGroupBuilderEjemplo = this.formBuilderEjemplo.group({
      campoEjemplo: this.formBuilderEjemplo.array([]) /*Form Builder ya incluye FormArray */
    });
  }

  /*Sintáxis de getters/setter de typescript
  https://stackoverflow.com/questions/12827266/get-and-set-in-typescript
  Obtener el array 
  */
  get campoEjemploArray(): FormArray  {

    /**
     * as - Es para hacer un cast de un tipo a otro
     * 
     * formGroupBuilderEjemplo.get('campoEjemplo') es un campoConstruido tipo FormArray, en return siguiente se lo castea a FormArray
     */
    return this.formGroupBuilderEjemplo.get('campoEjemplo') as FormArray;
  }

  /*Agregar campos al Array*/
  agregarCampos(){
    const grupoDeCampos = this.formBuilderEjemplo.group({
      campoFormularioUno: new FormControl(''),
      campoFormularioDos: new FormControl(''),
      campoFormularioTres: new FormControl(''),
      // email: ['john@angular.io', [
      //   Validators.required, Validators.email
      // ]],
      // password: ['', [
      //   Validators.required, Validators.minLength(4)
      // ]]
    });

    /*Hasta this.campo es una llamada a la funcion, push es añadile grupoDeCampos al array*/
    this.campoEjemploArray.push(grupoDeCampos);
  }


  /*Eliminar campos del array */
  eliminarCampos(indice: number) {
    this.campoEjemploArray.removeAt(indice);
  }


  ngOnInit(): void {
    
  }

  onSubmit(){
    console.log("Datos de los campos impresos en consola: ", this.formGroupEjemplo.value)
  }


}

