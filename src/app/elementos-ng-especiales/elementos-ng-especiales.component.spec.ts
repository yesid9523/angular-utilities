import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementosNgEspecialesComponent } from './elementos-ng-especiales.component';

describe('ElementosNgEspecialesComponent', () => {
  let component: ElementosNgEspecialesComponent;
  let fixture: ComponentFixture<ElementosNgEspecialesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ElementosNgEspecialesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ElementosNgEspecialesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
