import { Component, OnInit } from '@angular/core';
import { Satelite } from '../shared/interfaces/satelite';

@Component({
  selector: 'app-elementos-ng-especiales',
  templateUrl: './elementos-ng-especiales.component.html',
  styleUrls: ['./elementos-ng-especiales.component.scss']
})
export class ElementosNgEspecialesComponent implements OnInit {

  satelites: Satelite[] = [
    {
      id: 1,
      nombre: 'Luna',
      planeta: 'Tierra'
    },
    {
      id:2,
      nombre: 'Europa',
      planeta: 'Jùpiter'
    },
    {
      id: 3,
      nombre: 'Titàn',
      planeta: 'Saturno'
    },
    {
      id: 4,
      nombre: 'Crèsida',
      planeta: 'Urano'
    },
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
