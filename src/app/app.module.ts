import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InterpolacionComponent } from './interpolacion/interpolacion.component';
import { TemplatevariablesComponent } from './templatevariables/templatevariables.component';
import { PropertybindingComponent } from './propertybinding/propertybinding.component';
import { EventbindingComponent } from './eventbinding/eventbinding.component';
import { TwowaybindingComponent } from './twowaybinding/twowaybinding.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectivaNgIfComponent } from './directiva-ng-if/directiva-ng-if.component';
import { DirectivaNgForComponent } from './directiva-ng-for/directiva-ng-for.component';
import { DirectivaNgStyleComponent } from './directiva-ng-style/directiva-ng-style.component';
import { DirectivaNgClassComponent } from './directiva-ng-class/directiva-ng-class.component';
import { ComunicacionPadreAHijoComponent } from './comunicacion-padre-a-hijo/comunicacion-padre-a-hijo.component';
import { ComunicacionHijoAPadreComponent } from './comunicacion-hijo-a-padre/comunicacion-hijo-a-padre.component';
import { ComponenteHijoComponent } from './comunicacion-padre-a-hijo/componente-hijo/componente-hijo.component';
import { ComponenteHijoDosComponent } from './comunicacion-hijo-a-padre/componente-hijo/componente-hijo-dos.component';
import { ComunicacionBidirecionalPadreHijoComponent } from './comunicacion-bidirecional-padre-hijo/comunicacion-bidirecional-padre-hijo.component';
import { ServiciosManejoComponent } from './servicios-manejo/servicios-manejo.component';
import { ServicioEjemploService } from './shared/services/servicio-ejemplo.service';
import { DistanciaService } from './shared/services/distancia.service';
import { ElementosNgEspecialesComponent } from './elementos-ng-especiales/elementos-ng-especiales.component';
import { RoutingComponent } from './routing/routing.component';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LibreriaFormlyComponent } from './libreria-formly/libreria-formly.component';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormulariosAngularComponent } from './formularios-angular/formularios-angular.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'interpolacion', component: InterpolacionComponent },
  { path: 'template-variables', component: TemplatevariablesComponent },
  { path: 'propery-binding', component: PropertybindingComponent },
  { path: 'two-way-binding', component: TwowaybindingComponent },
  { path: 'directiva-ngif', component: DirectivaNgIfComponent },
  { path: 'directiva-ngfor', component: DirectivaNgForComponent },
  { path: 'directiva-ngstyle', component: DirectivaNgStyleComponent },
  { path: 'directiva-ngclass', component: DirectivaNgClassComponent },
  { path: 'comunicacion-padre-a-hijo', component: ComunicacionPadreAHijoComponent },
  { path: 'comunicacion-hijo-a-padre', component: ComunicacionHijoAPadreComponent },
  { path: 'servicios', component: ServiciosManejoComponent },
  { path: 'elementos-renderizables-especiales', component: ElementosNgEspecialesComponent },
  { path: 'comunicacion-bidireccional', component: ElementosNgEspecialesComponent },
  { path: 'routing', component: RoutingComponent },
  { path: 'libreria-formly', component: LibreriaFormlyComponent },
  { path: 'formularios-angular', component: FormulariosAngularComponent },


]

@NgModule({
  declarations: [
    AppComponent,
    InterpolacionComponent,
    TemplatevariablesComponent,
    PropertybindingComponent,
    EventbindingComponent,
    TwowaybindingComponent,
    DirectivaNgIfComponent,
    DirectivaNgForComponent,
    DirectivaNgStyleComponent,
    DirectivaNgClassComponent,
    ComunicacionPadreAHijoComponent,
    ComunicacionHijoAPadreComponent,
    ComponenteHijoComponent,
    ComponenteHijoDosComponent,
    ComunicacionBidirecionalPadreHijoComponent,
    ServiciosManejoComponent,
    ElementosNgEspecialesComponent,
    HomeComponent,
    RoutingComponent,
    LibreriaFormlyComponent,
    FormulariosAngularComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    FormlyModule.forRoot(),
    FormlyBootstrapModule,
    RouterModule.forRoot(routes)
  ],
  providers: [ServicioEjemploService, DistanciaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
