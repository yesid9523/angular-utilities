import { Injectable } from '@angular/core';
import { Satelite } from '../interfaces/satelite';
import { DistanciaService } from './distancia.service';

@Injectable({
  providedIn: 'root'
})
export class ServicioEjemploService {

  private satelites: Satelite[] = [
    {
      id: 1,
      nombre: 'Luna',
      planeta: 'Tierra'
    },
    {
      id:2,
      nombre: 'Europa',
      planeta: 'Jùpiter'
    },
    {
      id: 3,
      nombre: 'Titàn',
      planeta: 'Saturno'
    },
    {
      id: 4,
      nombre: 'Crèsida',
      planeta: 'Urano'
    },
  ]
  constructor(private distanciaServicio: DistanciaService) { }

  agregarSateliteServicio(satelite: Satelite){
    this.satelites.push(satelite);
  }

  obtenerSatelitesServicio(): Satelite[]{
    return this.satelites;
  }

  obtenerDistancia(cadena: string) {
    return this.distanciaServicio.obtenerDistanciaAPlaneta(cadena);
  }

}
