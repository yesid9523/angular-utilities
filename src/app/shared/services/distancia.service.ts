import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DistanciaService {

  distancia: String;
  constructor() { }

  obtenerDistanciaAPlaneta(satelite: string){
    if(satelite === 'Luna'){
      return 'a 400.000Km de la tierra';
    }
    return 'Sin coincidencias';
  }
}
